package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/samisagit/snitch/logger"
	"github.com/samisagit/snitch/server"
	"log"
	"net/http"
)

var port = "9090"

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	// Get ready for the logs
	go logger.PersistLogs(
		logger.HighPriorityQueue,
		logger.LowPriorityQueue,
		logger.Save,
		make(chan string))

	// Set up api routes
	r := mux.NewRouter()
	server.ConfigureRoutes(r)
	http.Handle("/", r)

	// Start the server on port 9090
	log.Printf("api listening on port %s\n", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
