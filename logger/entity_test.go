package logger

import (
	"testing"
	"time"
)

var logOrder = []Log{}

func saveIntoSlice(l Log) error {
	logOrder = append(logOrder, l)
	return nil
}

func TestProdQueues(t *testing.T) {
	for i := 0; i < queueLength; i++ {
		HighPriorityQueue <- Log{Severity: "fatal"}
		LowPriorityQueue <- Log{Severity: "info"}
	}
	killer := make(chan string)
	go PersistLogs(HighPriorityQueue, LowPriorityQueue, saveIntoSlice, killer)
	time.Sleep(1 * time.Millisecond)
	killer <- ""
	for i, val := range logOrder {
		if i < queueLength && val.Severity != "fatal" {
			t.Errorf("queues not prioritised correctly, expected index %d to be fatal, got %v", i, val)
		} else if i >= queueLength && val.Severity != "info" {
			t.Errorf("queues not prioritised correctly, expected index %d to be info, got %v", i, val)
		}
	}

}
