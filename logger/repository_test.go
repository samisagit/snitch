package logger

import (
	"errors"
	"testing"
	"time"
)

var logCount = 0

var persistLogsParams = []struct {
	testCase       string
	lowLogsToTest  int
	highLogsToTest int
	killers        int
	hpq            chan Log
	lpq            chan Log
	killer         chan string
}{
	{"high priority", 0, 4, 0, make(chan Log, 5), make(chan Log, 5), make(chan string, 1)},
	{"low priority", 4, 0, 0, make(chan Log, 5), make(chan Log, 5), make(chan string, 1)},
	{"mixed priority", 2, 2, 1, make(chan Log, 5), make(chan Log, 5), make(chan string, 1)},
	{"panic in queue", 0, 0, 0, make(chan Log, 5), make(chan Log, 5), make(chan string, 1)},
}

func mockSaver(_ Log) error {
	logCount++
	return nil
}

func mockPanicSaver(_ Log) error {
	return errors.New("Test log save error")
}

func TestQueue(t *testing.T) {
	for _, tt := range persistLogsParams {
		switch tt.testCase {
		case "panic in queue":
			t.Run("Queue panic", func(t *testing.T) {
				defer func() {
					if r := recover(); r == nil {
						t.Error("error in log saver should cause PersistLogs to panic")
					}
				}()
				tt.lpq <- Log{}
				PersistLogs(tt.hpq, tt.lpq, mockPanicSaver, tt.killer)
			})
		case "high priority":
			t.Run("Queue (high priority)", func(t *testing.T) {
				go PersistLogs(tt.hpq, tt.lpq, mockSaver, tt.killer)
				for i := 0; i < tt.highLogsToTest; i++ {
					tt.hpq <- Log{}
				}
				time.Sleep(1 * time.Millisecond)
				if logCount < tt.highLogsToTest {
					t.Errorf("only persisted %d of %d logs", logCount, tt.highLogsToTest)
				}
			})
		case "low priority":
			t.Run("Queue (low priority)", func(t *testing.T) {
				go PersistLogs(tt.hpq, tt.lpq, mockSaver, tt.killer)
				for i := 0; i < tt.lowLogsToTest; i++ {
					tt.lpq <- Log{}
				}
				time.Sleep(1 * time.Millisecond)
				if logCount < tt.lowLogsToTest {
					t.Errorf("only persisted %d of %d logs", logCount, tt.lowLogsToTest)
				}
			})
		case "mixed priority":
			t.Run("Queue (mixed priority)", func(t *testing.T) {
				for i := 0; i < tt.lowLogsToTest; i++ {
					tt.lpq <- Log{}
				}
				tt.killer <- ""
				go PersistLogs(tt.hpq, tt.lpq, mockSaver, tt.killer)
				time.Sleep(1 * time.Millisecond)
				if len(tt.lpq) < tt.lowLogsToTest {
					t.Errorf("does not prioritise logs - sent %d of %d low priority logs in error", (tt.lowLogsToTest - len(tt.lpq)), tt.lowLogsToTest)
				}
			})
		}
	}
}
