package logger

type Log struct {
	id          int    `json:id`
	appId       string `json:appName`
	Severity    string `json:severity`
	Message     string `json:message`
	Environment string `json:environment`
	Time        int32  `json:time`
	Stack       string `json:stack`
}

var queueLength = 1000
var HighPriorityQueue = make(chan Log, queueLength)
var LowPriorityQueue = make(chan Log, queueLength)

// Getters
func (l Log) Id() int {
	return l.id
}

func (l Log) AppId() string {
	return l.appId
}

func (l Log) GetEnvironment() string {
	return l.Environment
}

func (l Log) GetMessage() string {
	return l.Message
}

func (l Log) GetTime() int32 {
	return l.Time
}

// Setters
func (l *Log) SetAppId(appId string) {
	l.appId = appId
}
