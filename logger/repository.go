package logger

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/samisagit/snitch/config"
	"log"
)

type mysqlDB struct {
	conn *sql.DB
}

type saver func(Log) error

var db mysqlDB

func init() {
	dbCreds := config.MySqlUser +
		":" +
		config.MySqlPassword +
		"@tcp(" +
		config.MySqlHost +
		")/" +
		"logger"
	pool, err := sql.Open("mysql", dbCreds)
	if err != nil || pool == nil {
		log.Fatal(err)
	}
	connectToDatabase(pool)
}

func connectToDatabase(p *sql.DB) {
	db = mysqlDB{
		conn: p,
	}
}

func PersistLogs(highPriorityChan chan Log, lowPriorityChan chan Log, saveFunc saver, killswitch chan string) {
	var nextLog Log
	for {
		if len(highPriorityChan) > 0 || len(killswitch) > 0 {
			select {
			case <-killswitch:
				return
			case nextLog = <-highPriorityChan:
			}
		} else {
			select {
			case <-killswitch:
				return
			case nextLog = <-highPriorityChan:
			case nextLog = <-lowPriorityChan:
			}
		}
		err := saveFunc(nextLog)
		if err != nil {
			panic(err)
		}
	}
}

func Save(l Log) error {
	queryString := `
		INSERT INTO logs (
			app,
			message,
			severity,
			environment,
			time,
			stackTrace)
		VALUES (?, ?, ?, ?, ?, ?);`
	_, err := db.conn.Exec(
		queryString,
		l.AppId(),
		l.Message,
		l.Severity,
		l.Environment,
		l.Time,
		l.Stack)

	return err
}

func FindOne(id int) Log {
	queryString := `
		SELECT * FROM logs
		WHERE id = ?;`

	var logId int
	var app string
	var message string
	var severity string
	var environment string
	var time int32
	var stackTrace string
	err := db.conn.QueryRow(queryString, id).Scan(
		&logId,
		&app,
		&message,
		&severity,
		&environment,
		&time,
		&stackTrace)
	if err != nil {
		log.Println(err)
		panic("error getting result from persistent storage")
	}
	return Log{
		id:          logId,
		appId:       app,
		Message:     message,
		Severity:    severity,
		Environment: environment,
		Time:        time,
		Stack:       stackTrace,
	}
}

func FindPaginated(filters string) []Log {
	logs := []Log{}
	var logId int
	var app string
	var message string
	var severity string
	var environment string
	var time int32
	var stackTrace string
	queryString := `
		SELECT * FROM logs
		WHERE id IS NOT NULL` +
		filters +
		` ORDER BY time`
	rows, err := db.conn.Query(queryString)
	if err != nil {
		log.Println(err)
		panic("error getting result from persistent storage")
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(
			&logId,
			&app,
			&message,
			&severity,
			&environment,
			&time,
			&stackTrace)
		if err != nil {
			log.Fatal(err)
		}
		logs = append(logs, Log{
			id:          logId,
			appId:       app,
			Message:     message,
			Severity:    severity,
			Environment: environment,
			Time:        time,
			Stack:       stackTrace,
		})
	}
	return logs
}
