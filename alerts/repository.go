package alerts

import "github.com/samisagit/snitch/lib/slack"
import (
	"fmt"
)

func ConvertLog(d dispatchee) slack.Message {
	message := slack.NewMessage()
	content := fmt.Sprintf(`[
		{
			"fallback": "Error from %s",
			"color": "danger",
			"mrkdwn_in": [
				"fields"
			],
			"fields": [
				{
					"title": "App",
					"value": "%s",
					"short": true
				},
				{
					"title": "Environment",
					"value": "%s",
					"short": true
				},
				{
					"title": "Message",
					"value": "%s",
					"short": false
				}
			],
			"ts": %d
		},
		{
			"fallback": "Error button",
			"actions": [
				{
					"type": "button",
					"text": "View error",
					"url": "https://logger.com/logs/%s"
				}
			]
		}
	]`,
		d.AppId(),
		d.AppId(),
		d.GetEnvironment(),
		d.GetMessage(),
		d.GetTime(),
		d.Id())
	message.AddPre("attachments", content)
	message.AddString("channel", "alerts")
	return message
}
