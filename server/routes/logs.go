package routes

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"github.com/samisagit/snitch/auth"
	"github.com/samisagit/snitch/logger"
	"github.com/samisagit/snitch/middleware"
)

func LogRoutes(r *mux.Router) {
	r.Use(middleware.RoutePanic, middleware.Cors, middleware.Jwt)

	r.PathPrefix("/{id}").Methods("GET").HandlerFunc(getLog).Name("logs")
	r.PathPrefix("").Methods("GET").HandlerFunc(getPaginatedLog).Name("logs")
	r.PathPrefix("").Methods("POST").HandlerFunc(commitLog).Name("log")
}

func commitLog(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	jwt := r.Header.Get("Authorization")

	accountId, err := auth.GetAccountIdFromPayload(jwt)
	logger.ReceiveLog(body, strconv.Itoa(accountId))
	w.WriteHeader(http.StatusOK)
}

func getLog(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	idInt, err := strconv.Atoi(id)

	l := logger.RetrieveLog(idInt)
	jsonLog, err := json.Marshal(l)
	if err != nil {
		panic(err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(jsonLog))
}

func getPaginatedLog(w http.ResponseWriter, r *http.Request) {
	filters := buildLogFilters(r) + buildTimeSliceFilter(r)
	logs := logger.RetrievePaginatedLogs(filters)
	jsonSlice := make([]string, 0)
	for _, l := range logs {
		jsonLog, err := json.Marshal(l)
		if err != nil {
			panic(err)
		}
		jsonSlice = append(jsonSlice, string(jsonLog))
	}
	jsonBlob := "[" + strings.Join(jsonSlice, ",") + "]"
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(jsonBlob))
}

func buildLogFilters(r *http.Request) string {
	envs := r.FormValue("environment")
	sevs := r.FormValue("severity")

	m := make(map[string][]string)
	m["environment"] = strings.Split(envs, ",")
	m["severity"] = strings.Split(sevs, ",")

	queryStringAdditions := ""
query:
	for k, v := range m {
		if len(v) < 1 {
			continue
		}
		for i, value := range m[k] {
			if len(value) < 1 {
				break query
			}
			m[k][i] = strconv.Quote(value)
		}
		queryStringAdditions += " AND " + k + " IN (" + strings.Join(v, ",") + ")"
	}

	return queryStringAdditions
}

func buildTimeSliceFilter(r *http.Request) string {
	timeSlice := r.FormValue("timeslice")
	times := strings.Split(timeSlice, ",")
	if len(times) < 2 {
		return ""
	}
	return " AND time >= " + times[0] + " AND time <= " + times[1]
}
