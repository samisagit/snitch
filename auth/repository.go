package auth

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/samisagit/snitch/config"
	"log"
	"strconv"
)

type mysqlDB struct {
	conn *sql.DB
}

var db mysqlDB

func init() {
	dbCreds := config.MySqlUser +
		":" +
		config.MySqlPassword +
		"@tcp(" +
		config.MySqlHost +
		")/" +
		"logger"
	pool, err := sql.Open("mysql", dbCreds)
	if err != nil || pool == nil {
		log.Fatal(err)
	}
	connectToDatabase(pool)
}

func connectToDatabase(p *sql.DB) {
	db = mysqlDB{
		conn: p,
	}
}

func GetPassword(id int) (string, error) {
	queryString := `
		SELECT password
		FROM users
		WHERE id = ` + strconv.Itoa(id)
	var password string
	err := db.conn.QueryRow(queryString).Scan(&password)
	if err != nil {
		return "", err
	}
	return password, nil
}
