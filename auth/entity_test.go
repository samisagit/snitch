package auth

import "testing"

func TestHashStringSame(t *testing.T) {
	a := HashString("test", "hash")
	b := HashString("test", "hash")
	c := a == b
	if !c {
		t.Error("func HashString should return the same value given the same params")
	}
}

func TestHashStringDiffValue(t *testing.T) {
	a := HashString("test", "hash")
	b := HashString("check", "hash")
	c := a == b
	if c {
		t.Error("func HashString should return a different value given different params")
	}
}

func TestHashStringDiffHash(t *testing.T) {
	a := HashString("test", "hash")
	b := HashString("test", "hahs")
	c := a == b
	if c {
		t.Error("func HashString should return a different value given different hashes")
	}
}

func TestAuthenticateJwtSameHash(t *testing.T) {
	j := jwt{}
	token := j.toString("test")
	if !AuthenticateJwt(token, "test") {
		t.Error("jwt authentication should pass with same secret")
	}
}

func TestAuthenticateJwtDiffHash(t *testing.T) {
	j := jwt{}
	token := j.toString("test")
	if AuthenticateJwt(token, "tset") {
		t.Error("jwt authentication should fail with different secret")
	}
}

func TestAuthenticateJwtDiffValue(t *testing.T) {
	j := jwt{}
	token := j.toString("test")
	token = token + "extra stuff"
	if AuthenticateJwt(token, "test") {
		t.Error("jwt authentication should fail with triffled token")
	}
}

func TestToStringSameVal(t *testing.T) {
	j := jwt{}
	jT := j.toString("test")
	k := jwt{}
	kT := k.toString("test")
	if jT != kT {
		t.Error("jwt hashed with same salt should be the same")
	}
}

func TestToStringSections(t *testing.T) {
	j := jwt{}
	jT := j.toString("test")
	if !AuthenticateJwt(jT, "test") {
		t.Error("toString should return valid jwt")
	}
}

func TestJwtLifeCycle(t *testing.T) {
	ar := authRequest{
		AccountId: 123,
		Password:  "password",
	}
	jwt := ar.generateJwt().toString("test")
	if !AuthenticateJwt(jwt, "test") {
		t.Error("jwt generated or parsed incorrectly")
	}
}
