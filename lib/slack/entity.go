package slack

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/samisagit/snitch/config"
	"io/ioutil"
	"net/http"
	"strconv"
)

type Message struct {
	content          string
	dispatchAttempts int
}

func NewMessage() Message {
	return Message{
		"{}",
		0,
	}
}

func (m *Message) AddString(key string, value string) {
	p := ""
	if m.content != "{}" {
		p = ", "
	}
	k := strconv.Quote(key)
	v := strconv.Quote(value)
	addition := fmt.Sprintf("%s: %s%s", k, v, p)
	m.content = m.content[:1] + addition + m.content[1:]
}

func (m *Message) AddInt(key string, value int) {
	p := ""
	if m.content != "{}" {
		p = ", "
	}
	k := strconv.Quote(key)
	addition := fmt.Sprintf("%s: %d%s", k, value, p)
	m.content = m.content[:1] + addition + m.content[1:]
}

func (m *Message) AddPre(key string, value string) {
	p := ""
	if m.content != "{}" {
		p = ", "
	}
	k := strconv.Quote(key)
	addition := fmt.Sprintf("%s: %s%s", k, value, p)
	m.content = m.content[:1] + addition + m.content[1:]
}

func (m *Message) AddBool(key string, value bool) {
	p := ""
	if m.content != "{}" {
		p = ", "
	}
	k := strconv.Quote(key)
	addition := fmt.Sprintf("%s: %t%s", k, value, p)
	m.content = m.content[:1] + addition + m.content[1:]
}

func (m Message) Send() error {
	var jsonStr = []byte(m.content)
	req, err := http.NewRequest(
		"POST",
		"https://slack.com/api/chat.postMessage",
		bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.SlackToken))
	client := &http.Client{}
	resp, err := client.Do(req)
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		return errors.New(string(bodyBytes))
	}
	return err
}
